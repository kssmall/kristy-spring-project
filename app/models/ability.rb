class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    if user.is? :dealership_owner
      can :manage_all
    elsif user.is? :inventory_manager
      # Define Inventory Manager's abilities here
    elsif user.is? :finance_manager
      # Define Finance Manager's abilities here
    elsif user.is? :sales_manager
      # Define Sales Manager's abilities here
    elsif user.is? :sales_person
      # Define Sales Person's abilities here
    end
  end
end
