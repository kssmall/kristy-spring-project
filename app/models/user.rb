class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Validations
  validates :first_name, :last_name, presence: true

  # Callbacks
  before_create :assign_default_role # used to assign default role to new user

  # User Roles
  ROLES = %w[sales_person sales_manager finance_manager inventory_manager dealership_owner]

  # Scopes (Custom Query)
  scope :with_role, ->(role) { where("roles_mask & ? > 0", 2**ROLES.index(role.to_s)) }

  def roles=(roles)
    self.roles_mask = (roles & ROLES).map { |r| 2**ROLES.index(r) }.inject(0, :+)
  end

  def roles
    ROLES.reject do |r|
      ((roles_mask.to_i || 0) & 2**ROLES.index(r)).zero?
    end
  end

  def is?(role)
    roles.include?(role.to_s)
  end

  private

  def assign_default_role
    self.roles =  ['sales_person'] unless roles_mask
  end

end
